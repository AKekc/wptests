<?php

/*
Plugin Name: Wptests

Description: Plugin for validate phone/mail.
Version: 1.0
Author: Kekc
License: A "Slug" license name e.g. GPL2
*/

include_once 'includes/settings.php';
$func = new functions;

function field_validate( $result ) {

    $validation_fields = get_option( 'validation_fields' );

    if( ! empty( $validation_fields['email'] ) && ! empty( $validation_fields['phone'] ) ) {

        $phone_field = $validation_fields['phone'];
        $email_field = $validation_fields['email'];

        $form  = WPCF7_Submission::get_instance();
        $phone = $form->get_posted_data( $phone_field );
        $email = $form->get_posted_data( $email_field );

        $error_msg = __('Заполните телефон или email');

        if( empty( $phone ) && empty( $email ) ) {

            $result->invalidate( $phone_field, $error_msg );
            $result->invalidate( $email_field, $error_msg );

        }

        return $result;

    }

}

add_filter( 'wpcf7_validate', 'field_validate' );
