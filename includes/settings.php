<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 27.03.2017
 * Time: 11:41
 */

class functions {
     private $public;

    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'validator_page' ) );
        add_action( 'admin_init', array( $this, 'create_setting' ) );

    }

    public function validator_page()
    {
        add_options_page('Validator for CF7','Validator for CF7','manage_options','validator-cf-7',array( $this, 'create_admin_page' ));
        add_options_page('Info validator for CF7','Info validator for CF7','manage_options','info-validator-cf-7',array( $this, 'info_admin_page' ));

    }

    public function create_admin_page()
    {
        $this->public = get_option( 'validation_fields' );
        ?>
        <div class="wrapper">
            <h1>Validation for CF7</h1>
            <form method="post" action="options.php">
                <?php
                settings_fields( 'validator_options' );
                do_settings_sections( 'validator-for-cf-7' );
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }
    public function info_admin_page(){
        include_once 'info.php';
    }

    public function create_setting()
    {
        register_setting('validator_options','validation_fields');

        add_settings_section('validation_field_section','Настройки валидатора контактной формы',array( $this, 'print_section_info' ),'validator-for-cf-7' );

        add_settings_field('phone','Имя поля "телефон":',array( $this, 'phone_callback' ),'validator-for-cf-7','validation_field_section');

        add_settings_field('email','Имя поля "email"',array( $this, 'email_callback' ),'validator-for-cf-7','validation_field_section');

    }


    public function print_section_info()
    {
        echo 'Заполните поля';
    }
    public function phone_callback()
    {
        ?>
        <input type="text" id="phone" name="validation_fields[phone]" value="<?php echo esc_attr( $this->public['phone'])?>" />
    <?php

    }
    public function email_callback()
    {
        ?>
        <input type="text" id="phone" name="validation_fields[email]" value="<?php echo esc_attr( $this->public['email'])?>" />
        <?php
    }


}
