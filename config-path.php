<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 14.02.2017
 * Time: 12:27
 */


define("VKW_PlUGIN_DIR", plugin_dir_path(__FILE__));
define("VKW_PlUGIN_DIR_LOCALIZATION", dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
define("VKW_PlUGIN_URL", plugin_dir_url( __FILE__ ));
define("VKW_PlUGIN_SLUG", preg_replace( '/[^\da-zA-Z]/i', '_',  basename(VKW_PlUGIN_DIR)));
define("VKW_PlUGIN_TEXTDOMAIN", str_replace( '_', '-', VKW_PlUGIN_SLUG ));
define("VKW_PlUGIN_OPTION_VERSION", VKW_PlUGIN_SLUG.'_version');
define("VKW_PlUGIN_OPTION_NAME", VKW_PlUGIN_SLUG.'_options');
define("VKW_PlUGIN_AJAX_URL", admin_url('admin-ajax.php'));
if ( ! function_exists( 'get_plugins' ) ) {
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
}
$TPOPlUGINs = get_plugin_data(VKW_PlUGIN_DIR.'/'.basename(VKW_PlUGIN_DIR).'.php', false, false);
define("VKW_PlUGIN_VERSION", $TPOPlUGINs['Version']);
define("VKW_PlUGIN_NAME", $TPOPlUGINs['Name']);

